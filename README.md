this is the code to grab the stats from wotcode and make pretty html
pages out of them

probably you want <https://wotmut.gitlab.io/stats>

the wotcode stats api is at <https://wotcode.eastus.cloudapp.azure.com/api/swagger/index.html>

the source code for the stat collecter is at <https://gitlab.com/wotmut/stats-collect>

dev docs are at <https://wotmut.gitlab.io/stats-collect/stats>

due to certain requirements, rust 1.62 or later may be required to build this.

a binary built in release mode is available at <https://wotmut.gitlab.io/stats-collect/stats-bin>
for x86_64 linux
