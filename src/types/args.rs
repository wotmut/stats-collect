//! Command line argument parsing

use std::path::PathBuf;

use argh::FromArgs;

/// Command line arguments
#[derive(Debug, FromArgs)]
pub struct Args {
	/// wotcode api url
	#[argh(option, default = "String::from(\"https://wotcodeapi.azurewebsites.net/api/Stats\")")]
	pub api_url: String,

	/// don't download anything
	#[argh(switch, short = 'd')]
	pub dont_download: bool,

	/// directory to write html to
	#[argh(option, default = "PathBuf::from(\"out\")")]
	pub out_dir: PathBuf,

	/// file to save json stats to
	#[argh(option, default = "PathBuf::from(\"stats.json\")")]
	pub stats_file: PathBuf,
}
