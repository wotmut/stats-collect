use std::fmt;

/// Type of extreme
pub enum ExtremeType {
	/// Minimum
	Minimum,

	/// Maximum
	Maximum,
}

impl fmt::Display for ExtremeType {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Maximum => write!(f, "Maximum"),
			Self::Minimum => write!(f, "Minimum"),
		}
	}
}

/// The extreme for all the stats
pub struct Extreme {
	/// Type of the extreme
	pub extreme_type: ExtremeType,

	/// Strength
	pub str: u8,

	/// Intelligence
	pub int: u8,

	/// Willpower
	pub wil: u8,

	/// Dexterity
	pub dex: u8,

	/// Constitution
	pub con: u8,

	/// Sum
	pub sum: u8,
}

impl Extreme {
	/// Create a new `Extreme` struct with `ExtremeType`
	pub fn new(extreme_type: ExtremeType) -> Self {
		Self { extreme_type, str: 0, int: 0, wil: 0, dex: 0, con: 0, sum: 0 }
	}
}

impl fmt::Display for Extreme {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"['{}', {}, {}, {}, {}, {}, {}],\n",
			&self.extreme_type, &self.str, &self.int, &self.wil, &self.dex, &self.con, &self.sum
		)
	}
}
