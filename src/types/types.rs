use std::collections::BTreeMap;

use crate::Stat;

/// the stats for a homeland
pub type Homeland = Vec<Stat>;

/// the homelands for a class
pub type Class = BTreeMap<String, Homeland>;

/// the classes for a race
pub type Race = BTreeMap<String, Class>;

/// all the races
pub type Races = BTreeMap<String, Race>;

/// sum of each stat
#[derive(Default)]
pub struct StatSums {
	pub str: u128,
	pub int: u128,
	pub wil: u128,
	pub dex: u128,
	pub con: u128,
	pub sum: u128,
}

/// Counts for each stat
#[derive(Default)]
pub struct StatCounts {
	pub str: BTreeMap<u8, u128>,
	pub int: BTreeMap<u8, u128>,
	pub wil: BTreeMap<u8, u128>,
	pub dex: BTreeMap<u8, u128>,
	pub con: BTreeMap<u8, u128>,
	pub sum: BTreeMap<u8, u128>,
}
