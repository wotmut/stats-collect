use crate::types::{Extreme, ExtremeType, Homeland, Stat, StatCounts, StatSums};

/// `Info` struct to hold min, max, sum, and count
pub struct Info {
	/// Minimum recorded stat
	pub min: Extreme,

	/// Maximum recorded stat
	pub max: Extreme,

	/// Count of each stat
	pub stat_counts: StatCounts,

	/// Sum of records per stat
	pub sum: StatSums,

	/// Total number of records
	pub count: u128,
}

impl From<&Homeland> for Info {
	fn from(h: &Homeland) -> Self {
		let mut info = Self::new();

		for stat in h {
			info.count += 1;

			info.cmp(&stat);

			if let Some(sc) = info.stat_counts.sum.get_mut(&stat.statSum) {
				*sc += 1;
			} else {
				info.stat_counts.sum.insert(stat.statSum, 1);
			}

			if let Some(sc) = info.stat_counts.str.get_mut(&stat.strength) {
				*sc += 1;
			} else {
				info.stat_counts.str.insert(stat.strength, 1);
			}

			if let Some(sc) = info.stat_counts.int.get_mut(&stat.intelligence) {
				*sc += 1;
			} else {
				info.stat_counts.int.insert(stat.intelligence, 1);
			}

			if let Some(sc) = info.stat_counts.wil.get_mut(&stat.willpower) {
				*sc += 1;
			} else {
				info.stat_counts.wil.insert(stat.willpower, 1);
			}

			if let Some(sc) = info.stat_counts.dex.get_mut(&stat.dexterity) {
				*sc += 1;
			} else {
				info.stat_counts.dex.insert(stat.dexterity, 1);
			}

			if let Some(sc) = info.stat_counts.con.get_mut(&stat.constitution) {
				*sc += 1;
			} else {
				info.stat_counts.con.insert(stat.constitution, 1);
			}
		}

		info
	}
}

impl Info {
	/// Create a new `Info` with everything zero
	pub fn new() -> Self {
		Self {
			min: Extreme::new(ExtremeType::Minimum),
			max: Extreme::new(ExtremeType::Maximum),
			stat_counts: StatCounts::default(),
			sum: StatSums::default(),
			count: 0,
		}
	}

	/// Prepare table for physical stats
	pub fn physicals(&self) -> String {
		let mut s = String::new();

		for i in 1..25 {
			if self.stat_counts.str.get(&i).is_some()
				|| self.stat_counts.dex.get(&i).is_some()
				|| self.stat_counts.con.get(&i).is_some()
			{
				s.push_str(&format!(
					"[{i}, {}, {}, {}],\n",
					self.stat_counts.str.get(&i).unwrap_or(&0),
					self.stat_counts.dex.get(&i).unwrap_or(&0),
					self.stat_counts.con.get(&i).unwrap_or(&0),
				));
			}
		}

		s
	}

	/// Prepare table for mental stats
	pub fn mentals(&self) -> String {
		let mut s = String::new();

		for i in 1..25 {
			if self.stat_counts.int.get(&i).is_some() || self.stat_counts.wil.get(&i).is_some() {
				s.push_str(&format!(
					"[{i}, {}, {}],\n",
					self.stat_counts.int.get(&i).unwrap_or(&0),
					self.stat_counts.wil.get(&i).unwrap_or(&0),
				));
			}
		}

		s
	}

	/// Compute the Averages line
	pub fn avg(&self) -> String {
		format!(
			"['Average', {}, {}, {}, {}, {}, {}],\n",
			(self.sum.str as f64 / self.count as f64 * 10.0).round() / 10.0,
			(self.sum.int as f64 / self.count as f64 * 10.0).round() / 10.0,
			(self.sum.wil as f64 / self.count as f64 * 10.0).round() / 10.0,
			(self.sum.dex as f64 / self.count as f64 * 10.0).round() / 10.0,
			(self.sum.con as f64 / self.count as f64 * 10.0).round() / 10.0,
			(self.sum.sum as f64 / self.count as f64 * 10.0).round() / 10.0,
		)
	}

	/// Compare to find min/max/sum
	pub fn cmp(&mut self, stat: &Stat) {
		self.min.str = lower(self.min.str, stat.strength);
		self.min.int = lower(self.min.int, stat.intelligence);
		self.min.wil = lower(self.min.wil, stat.willpower);
		self.min.dex = lower(self.min.dex, stat.dexterity);
		self.min.con = lower(self.min.con, stat.constitution);
		self.min.sum = lower(self.min.sum, stat.statSum);

		self.max.str = upper(self.max.str, stat.strength);
		self.max.int = upper(self.max.int, stat.intelligence);
		self.max.wil = upper(self.max.wil, stat.willpower);
		self.max.dex = upper(self.max.dex, stat.dexterity);
		self.max.con = upper(self.max.con, stat.constitution);
		self.max.sum = upper(self.max.sum, stat.statSum);

		self.sum.str += stat.strength as u128;
		self.sum.int += stat.intelligence as u128;
		self.sum.wil += stat.willpower as u128;
		self.sum.dex += stat.dexterity as u128;
		self.sum.con += stat.constitution as u128;
		self.sum.sum += stat.statSum as u128;
	}
}

/// return the lower of two numbers
fn lower(a: u8, b: u8) -> u8 {
	if a != 0 && a < b {
		a
	} else {
		b
	}
}

/// return the upper of two numbers
fn upper(a: u8, b: u8) -> u8 {
	if a > b {
		a
	} else {
		b
	}
}
