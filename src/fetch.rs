use rear::Result;
use serde_json::Value;
use ureq::get;

use crate::types::Args;

/// Fetch the JSON data from the API
pub fn fetch(args: &Args, last_id: u128) -> Result<Value> {
	// put limit and afterId into url
	let api_url = format!("{}?afterId={}&limit=9999999", &args.api_url, last_id);

	let res = match get(&api_url).call() {
		Ok(r) => r,
		Err(e) => {
			println!("Unable to fetch json");
			return Err(e.into());
		}
	};

	match res.into_json() {
		Ok(j) => Ok(j),
		Err(e) => {
			println!("Unable to parse json");
			Err(e.into())
		}
	}
}
