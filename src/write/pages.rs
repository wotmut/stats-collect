use std::fs::{create_dir_all, File};
use std::io::{BufWriter, Write};
use std::path::PathBuf;

use askama::Template;
use csv::Writer;
use rear::Result;
use serde_json::to_string_pretty;

use crate::types::{Args, Homeland, Info, Races};

#[derive(Template)]
#[template(path = "homeland.html")]
struct HomelandTemplate<'a> {
	stats: &'a Homeland,
	info: &'a Info,
	num_stats: usize,
}

/// write the html files
pub fn pages(args: &Args, races: &Races) -> Result<()> {
	//let mut path = PathBuf::from(STATS_OUT);
	let mut path = args.out_dir.clone();

	// each race gets a directory
	for (race_name, race) in races {
		path.push(&race_name);

		// each class gets a directory
		for (class_name, class) in race {
			path.push(&class_name);

			create_dir_all(&path)
				.map_err(|e| format!("Unable to create directory {}: {e}", &path.display()))?;

			// each homeland gets a file
			for (hname, homeland) in class {
				path.push(&hname);

				path.set_extension("html");
				write_html(&path, &homeland)?;

				path.set_extension("json");
				write_json(&path, &homeland)?;

				path.set_extension("csv");
				write_csv(&path, &homeland)?;

				path.set_extension("");
				path.pop();
			}

			path.pop();
		}

		path.pop();
	}

	Ok(())
}

fn write_html(path: &PathBuf, homeland: &Homeland) -> Result<()> {
	let info = Info::from(homeland);

	let mut stats = Vec::from(homeland.as_slice());
	stats.sort_by(|a, b| a.id.cmp(&b.id));
	stats.reverse();
	stats.truncate(250);

	let html_out = HomelandTemplate { stats: &stats, info: &info, num_stats: homeland.len() }
		.render()
		.map_err(|e| format!("Unable to render homeland template for {}: {e}", &path.display()))?;

	let f =
		File::create(&path).map_err(|e| format!("Unable to open file {}: {e}", &path.display()))?;
	let mut buf = BufWriter::new(f);

	buf.write_all(&html_out.as_bytes())
		.map_err(|e| format!("Unable to write to file {}: {e}", &path.display()))?;

	Ok(())
}

fn write_json(path: &PathBuf, homeland: &Homeland) -> Result<()> {
	let f =
		File::create(&path).map_err(|e| format!("Unable to open file {}: {e}", &path.display()))?;
	let mut buf = BufWriter::new(f);

	buf.write_all(
		to_string_pretty(&homeland).map_err(|e| format!("Unable to make json: {e}"))?.as_bytes(),
	)
	.map_err(|e| format!("Unable to write to file {}: {e}", &path.display()))?;

	Ok(())
}

fn write_csv(path: &PathBuf, homeland: &Homeland) -> Result<()> {
	let f =
		File::create(&path).map_err(|e| format!("Unable to open file {}: {e}", &path.display()))?;

	let mut buf = BufWriter::new(f);
	let mut wtr = Writer::from_writer(&mut buf);

	for row in homeland {
		wtr.serialize(&row).map_err(|e| format!("Unable to make csv from record: {row}: {e}"))?;
	}

	wtr.flush().map_err(|e| format!("Unable to write csv {}: {e}", &path.display()).into())
}
