use std::fs::File;

use rear::Result;

use crate::types::{Args, Stat};

/// write the stats json file
pub fn json(args: &Args, stats: &[Stat]) -> Result<()> {
	let path = args.stats_file.clone();

	serde_json::to_writer(
		File::create(&path)
			.map_err(|e| format!("Unable to create file {}: {e}", &path.display()))?,
		&stats,
	)
	.map_err(|e| format!("Unable to write to file {}: {e}", &path.display()).into())

	//Ok(())
}
