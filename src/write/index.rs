use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::PathBuf;

use askama::Template;
use chrono::{DateTime, Utc};
use rear::Result;

use crate::types::{Args, Races};

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
	count: usize,
	races: &'a Races,
	ts: DateTime<Utc>,
}

pub fn index(args: &Args, races: &Races, count: usize) -> Result<()> {
	let index = IndexTemplate { count, races: &races, ts: Utc::now() };

	let mut path: PathBuf = args.out_dir.clone();
	path.push("index.html");

	let f = match File::create(&path) {
		Ok(f) => f,
		Err(e) => {
			println!("Unable to open file: {}", &path.display());
			return Err(e.into());
		}
	};

	let mut buf = BufWriter::new(f);

	let html = match index.render() {
		Ok(h) => h,
		Err(e) => {
			println!("Unable to render index template");
			return Err(e.into());
		}
	};

	match buf.write_all(html.as_bytes()) {
		Ok(_) => Ok(()),
		Err(e) => {
			println!("Unable to write file: {}", &path.display());
			Err(e.into())
		}
	}
}
