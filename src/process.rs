//! Process JSON into `Stat` records

use crate::types::{Class, Race, Races, Stat};

/// go through the json and process the stats
pub fn process(stats: Vec<Stat>) -> Races {
	let mut races = Races::new();

	for stat in stats {
		if let Some(race) = races.get_mut(&stat.race) {
			if let Some(class) = race.get_mut(&stat.class) {
				if let Some(homeland) = class.get_mut(&stat.homeland) {
					homeland.push(stat);
				} else {
					class.insert(stat.homeland.clone(), vec![stat]);
				}
			} else {
				let mut class = Class::new();
				let c = stat.class.clone();
				class.insert(stat.homeland.clone(), vec![stat]);
				race.insert(c, class);
			}
		} else {
			let mut class = Class::new();
			let mut race = Race::new();

			let c = stat.class.clone();
			let r = stat.race.clone();

			class.insert(stat.homeland.clone(), vec![stat]);
			race.insert(c, class);
			races.insert(r, race);
		}
	}

	races
}
