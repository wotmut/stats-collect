//! Collect and parse stats from WoTCode

mod fetch;
mod process;
mod types;
mod write;

use chrono::Utc;
use rear::Result;

use fetch::fetch;
use process::process;
use types::{Args, Stat};

fn main() -> Result<()> {
	let start = Utc::now();

	let args: Args = argh::from_env();

	println!("Reading any existing JSON data...");
	let mut stats = Stat::from_json_file(&args.stats_file).unwrap_or(Vec::<Stat>::new());
	println!("{} records read", stats.len());

	// might be not in order so let's just run through them all real quick and get highest id
	let mut last_id = 0;
	for stat in &stats {
		if stat.id > last_id {
			last_id = stat.id;
		}
	}

	if !args.dont_download {
		println!("Fetching new records...");
		let new_stats: Vec<Stat> = serde_json::from_value(fetch(&args, last_id)?)?;
		println!("{} new records read", new_stats.len());

		if new_stats.len() > 0 {
			println!("Combining records...");
			stats.extend(new_stats);
		}
	}

	println!("Writing JSON data to file...");
	write::json(&args, &stats)?;

	println!("Processing stats...");
	let count = stats.len();
	let races = process(stats);

	println!("writing html files...");
	write::pages(&args, &races)?;

	println!("Creating index...");
	write::index(&args, &races, count)?;

	let elapsed = Utc::now() - start;

	println!(
		"Processed {count} stats in {}:{}:{}.{}",
		elapsed.num_hours(),
		elapsed.num_minutes() % 60,
		elapsed.num_seconds() % 60,
		elapsed.num_milliseconds() % 1000
	);

	Ok(())
}
