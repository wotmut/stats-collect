#!/bin/bash

cd ~/stats-collect || exit 11
cargo run --release -- --out-dir ~/stats/public || exit 21
cd ~/stats || exit 31
git add . || exit 41
git commit -m "$(date)" || exit 51
git push || exit 61
